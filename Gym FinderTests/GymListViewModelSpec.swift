//
//  Gym_FinderTests.swift
//  Gym FinderTests
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import XCTest
@testable import Gym_Finder

class GymListViewModelSpec: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

	func testGym() {
		let		g = Gym(name:"alpha",address:"beta");
		XCTAssertEqual( g.name, "alpha" );
		XCTAssertEqual( g.address, "beta" );
	}

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
