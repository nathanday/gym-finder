//
//  GymService.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation
import CoreLocation;

struct GymService : GymServiceProtocol {
	static let URLString = "https://private-anon-d5fc391325-fitlgdemo.apiary-mock.com/api/v1/gyms";

	let		url = URL(string:GymService.URLString)!;

	func body(for aLocation: CLLocationCoordinate2D ) -> Data {
		return try! JSONSerialization.data(withJSONObject: ["latitude": aLocation.latitude, "longitude": aLocation.longitude], options: [])
	}

	func requestGyms(fromLocation aLocation: CLLocationCoordinate2D, completion aCompletions: @escaping ([Gym]?, Error?) -> Void) {
		var		theURLRequest = URLRequest(url: url);
		theURLRequest.httpMethod = "POST";
		theURLRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
		theURLRequest.addValue("application/json", forHTTPHeaderField: "Accept")
		theURLRequest.httpBody = body(for:aLocation);

		let		theURLSession = URLSession.shared.dataTask(with: theURLRequest) {
			(aData: Data?, aResponse: URLResponse?, anError: Error?) in
			guard let theData = aData else {
				aCompletions(nil, anError ?? NSError(domain: "Failed to fetch", code: 2, userInfo: nil));
				return;
			}

			let theFactory = GymFactory(data: theData);
			if let theResult = theFactory.parse() {
				aCompletions(theResult, nil);
			} else {
				aCompletions(nil, anError ?? NSError(domain: "Failed to fetch", code: 2, userInfo: nil));
			}
		}
		theURLSession.resume();
	}
}
