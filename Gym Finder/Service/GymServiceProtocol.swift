//
//  GymServiceProtocol.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation
import CoreLocation;

protocol GymServiceProtocol {
	func requestGyms(fromLocation aLocation: CLLocationCoordinate2D, completion: @escaping ([Gym]?, Error?) -> Void);
}
