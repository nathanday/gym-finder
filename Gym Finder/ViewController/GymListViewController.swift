//
//  ViewController.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import UIKit

class GymListViewController: UIViewController, UITableViewDataSource {

	var		gymListViewModel: GymListViewModel?;
	var		tableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		gymListViewModel = GymListViewModel(service: GymService());
		gymListViewModel?.load {
			OperationQueue.main.addOperation { [weak self] in
				self?.tableView.reloadData();
			}
		};
	}

	// UITableViewDataSource methods

	public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return gymListViewModel?.everyGymListCellViewModel.count ?? 0;
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let		cellIdendifier = "GymListTableViewCell"
		let		theCell = tableView.dequeueReusableCell(withIdentifier: cellIdendifier, for: indexPath) as! GymListTableViewCell

		if let theGymModel = gymListViewModel?.everyGymListCellViewModel[indexPath.row] {
			theCell.display(for: theGymModel);
		}
		return theCell
	}
}

