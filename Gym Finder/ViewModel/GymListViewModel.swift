//
//  GymListViewModel.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation
import CoreLocation;

struct GymListViewModel {
	var		everyGymListCellViewModel = [GymListCellViewModel]();
	var		gymListViewModelCLLocationManagerDelegate: GymListViewModelCLLocationManagerDelegate?
	var		loadCompletion: (() -> Void)?

	init( service aSevice : GymServiceProtocol ) {
		gymListViewModelCLLocationManagerDelegate = GymListViewModelCLLocationManagerDelegate(service: aSevice, parent: self);
	}

	mutating func load(_ aCompletion: @escaping () -> Void) {
		loadCompletion = aCompletion;
		gymListViewModelCLLocationManagerDelegate?.load();
	}

	mutating func load(gyms aGyms: [Gym] ) {
		assert( loadCompletion != nil, "loadCompletion not set" );
		gymListViewModelCLLocationManagerDelegate = nil;
		for theGym in aGyms {
			everyGymListCellViewModel.append(GymListCellViewModel(gym: theGym));
		}
		loadCompletion?()
	}
}

class GymListViewModelCLLocationManagerDelegate: NSObject, CLLocationManagerDelegate {
	var		parent: GymListViewModel
	let		locationManager = CLLocationManager()
	let		service: GymServiceProtocol
	init(service aSevice : GymServiceProtocol, parent aParent: GymListViewModel) {
		parent = aParent;
		service = aSevice;
	}


	func load() {
		locationManager.requestAlwaysAuthorization()

		// For use in foreground
		locationManager.requestWhenInUseAuthorization()

		if CLLocationManager.locationServicesEnabled() {
			locationManager.delegate = self
			locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
			locationManager.startUpdatingLocation()
		}
	}

	deinit {
		locationManager.stopUpdatingLocation();
	}

	func locationManager(_ aManager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
		if let theLocation: CLLocationCoordinate2D = aManager.location?.coordinate {
			service.requestGyms(fromLocation: theLocation) { (aGyms: [Gym]?, anError ) in
				if let theGyms = aGyms {
					self.parent.load(gyms: theGyms);
				} else {
					print( anError ?? "Error" );
				}
			}
		}
	}
}

