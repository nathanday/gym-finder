//
//  GymListCellViewModel.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation

struct GymListCellViewModel {
	let		name: String;
	let		address: String;

	init( gym aGym: Gym ) {
		name = aGym.name;
		address = aGym.address;
	}
	
}
