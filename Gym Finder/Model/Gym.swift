//
//  Gym.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation

struct Gym {
	let		name: String;
	let		address: String;
}
