//
//  GymFactory.swift
//  Gym Finder
//
//  Created by Nathan Day on 19/1/19.
//  Copyright © 2019 Nathaniel Day. All rights reserved.
//

import Foundation
import SwiftyJSON

struct GymFactory {
	let		data: Data;
	init(data aData: Data ) {
		data = aData;
	}

	/*
	we could improve this with an error result
	*/
	func parse() -> [Gym]? {
		guard let gymsList = try? JSON(data: data) else {
			return nil
		}
		var		theGymList = [Gym]();
		for (_,theGymElement):(String, JSON) in gymsList {
			if let theName = theGymElement["name"].string,
				let theAddress = theGymElement["address"].string {
				theGymList.append(Gym(name:theName,address:theAddress));
			}
		}

		return theGymList;
	}
}
